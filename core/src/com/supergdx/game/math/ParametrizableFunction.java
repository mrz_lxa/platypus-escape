package com.supergdx.game.math;

import java.nio.FloatBuffer;

/**
 * Created by alexey.moroz on 3/20/18.
 */

public interface ParametrizableFunction {

    /**
     * @return parameters buffer size in bytes.
     */
    int getParamsBufferSize();


    /**
     * Assume all function parameters to be float values,
     * represent parameters of a function as an array of floats being put to a FloatBuffer.
     *
     * @return FloatBuffer containing parameters of a function.
     */
    FloatBuffer getParamsBuffer();
}
