package com.supergdx.game.math.functions;

import com.badlogic.gdx.math.Vector2;
import com.supergdx.game.math.MFunction;

/**
 * Created by alexey.moroz on 3/21/18.
 */

public class ShortestDistanceFunction implements MFunction{


    private float px;
    private float py;

    private MFunction func;

    public ShortestDistanceFunction(MFunction func, float px, float py){
        this.func = func;

        this.px = px;
        this.py = py;
    }

    public ShortestDistanceFunction(MFunction func){
        this(func, 0.0f, 0.0f);
    }

    @Override
    public float val(float x) {
        return py * func.der(x) - func.val(x)*func.der(x) + px - x;
    }

    @Override
    public float der(float x) {
        return func.der2(x) * (py - func.val(x)) - func.der(x) * func.der(x) - 1.0f;
    }

    @Override
    public float der2(float x) {
        return 0;
    }

   public void setPoint(float px, float py){
       this.px = px;
       this.py = py;
   }

   public void setPoint(Vector2 vec){
       this.px = vec.x;
       this.py = vec.y;
   }

}
