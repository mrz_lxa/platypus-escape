package com.supergdx.game.math.functions;

import com.supergdx.game.math.MFunction;
import com.supergdx.game.math.ParametrizableFunction;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by alexey.moroz on 3/20/18.
 */

public class ComplexSineFunction implements MFunction, ParametrizableFunction{

    private static final int FLOAT_BYTE_COUNT = 4;

    private static final float TWO_PI = (float) Math.PI*2;

    private SineFunction a; //function of AMPLITUDE
    private SineFunction f; //function of FREQUENCY
    private SineFunction p; //function of PHASE
    private SineFunction c; //function of SHIFT (CONSTANT)

    private FloatBuffer paramsFloatBuffer;

    public ComplexSineFunction(SineFunction ampFunc, SineFunction freqFunc, SineFunction phaseFunc, SineFunction shiftFunc) {
        this.a = ampFunc;
        this.f = freqFunc;
        this.p = phaseFunc;
        this.c = shiftFunc;

        initFloatBuffer();
    }

    private void initFloatBuffer(){

        int byteCountToAllocate = 0;


        FloatBuffer ampFuncBuffer = a.getParamsBuffer();
        FloatBuffer freqFuncBuffer = f.getParamsBuffer();
        FloatBuffer phaseFuncBuffer = p.getParamsBuffer();
        FloatBuffer constFuncBuffer = c.getParamsBuffer();

        byteCountToAllocate += ampFuncBuffer.capacity() * FLOAT_BYTE_COUNT;
        byteCountToAllocate += freqFuncBuffer.capacity() * FLOAT_BYTE_COUNT;
        byteCountToAllocate += phaseFuncBuffer.capacity() * FLOAT_BYTE_COUNT;
        byteCountToAllocate += constFuncBuffer.capacity() * FLOAT_BYTE_COUNT;


        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(byteCountToAllocate);
        byteBuffer.order(ByteOrder.nativeOrder());

        paramsFloatBuffer = byteBuffer.asFloatBuffer();
        paramsFloatBuffer.put(ampFuncBuffer);
        paramsFloatBuffer.put(freqFuncBuffer);
        paramsFloatBuffer.put(phaseFuncBuffer);
        paramsFloatBuffer.put(constFuncBuffer);
        paramsFloatBuffer.position(0);
    }


    @Override
    public float val(float x) {
        return a.val(x) * sin( f.val(x) * x + p.val(x)) + c.val(x);
    }


    public float getPeriodAt(float x){
        return TWO_PI / f.val(x);
    }

    /**
     * First order derivative of a complex sine function.
     * Calculated on paper (may use wolfram alpha web for a check).
     *
     * @param x
     * @return Value of first order derivative at x coord.
     */
    @Override
    public float der(float x) {
        return a.der(x) * sin( f.val(x) * x + p.val(x) )
                + a.val(x) * ( x * f.der(x) + f.val(x) + p.der(x) ) * cos( f.val(x) * x + p.val(x))
                + c.der(x);
    }



    /**
     * Second order derivative of a complex sine function.
     * Calculated on paper (may use wolfram alpha web for a check).
     *
     * @param x
     * @return Value of first order derivative at x coord.
     */
    @Override
    public float der2(float x) {
        return a.der2(x) * sin(x * f.val(x) + p.val(x))
                + 2.0f * a.der(x) * (x * f.der(x) + f.val(x) + p.der(x)) * cos( x * f.val(x) + p.val(x) )
                - a.val(x) * ( x * f.der(x) + f.val(x) + p.der(x) ) * ( x * f.der(x) + f.val(x) + p.der(x) ) * sin(x * f.val(x) + p.val(x))
                + a.val(x) * cos( x * f.val(x) + p.val(x) ) * ( x * f.der2(x) + 2 * f.der(x) + p.der2(x) ) + c.der2(x);
    }



    private float sin(float x){
        return (float) Math.sin(x);
    }

    private float cos(float x){
        return (float) Math.cos(x);
    }


    @Override
    public int getParamsBufferSize() {
        return a.getParamsBufferSize()
                + f.getParamsBufferSize()
                + p.getParamsBufferSize()
                + c.getParamsBufferSize();
    }

    /**
     * Return FloatBuffer containing parameters that describe this sine wave.
     * Order: [Amplitude function buffer values, Frequency function buffer values, Phase function buffer values, Constant function buffer values]
     *
     *
     * @return Read-only float buffer.
     */
    public FloatBuffer getParamsBuffer(){
        //return read-only buffer in order to prevent occasional changes.
        return paramsFloatBuffer.asReadOnlyBuffer();
    }

}
