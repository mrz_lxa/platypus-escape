package com.supergdx.game.math.functions;

import com.supergdx.game.math.MFunction;


/**
 * Created by alexey.moroz on 8/10/18.
 */

public class LinearFunction implements MFunction {

    private float k;
    private float b;


    public LinearFunction(float k, float b){
        this.k = k;
        this.b = b;
    }

    @Override
    public float val(float x) {
        return k * x + b;
    }

    @Override
    public float der(float x) {
        return k;
    }

    @Override
    public float der2(float x) {
        return 0;
    }
}
