package com.supergdx.game.math.functions;

import com.supergdx.game.math.MFunction;

/**
 * Created by alexey.moroz on 12/25/18.
 */

public class LinearCompositionFunction implements MFunction {

    private float k;
    private float b;
    private MFunction valueFunc;


    public LinearCompositionFunction(MFunction valueFunc, float k, float b){
        this.valueFunc = valueFunc;
        this.k = k;
        this.b = b;
    }

    @Override
    public float val(float x) {
        return k * valueFunc.val(x) + b;
    }

    @Override
    public float der(float x) {
        return k * valueFunc.der(x);
    }

    @Override
    public float der2(float x) {
        return k * valueFunc.der2(x);
    }
}
