package com.supergdx.game.objects;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.supergdx.game.collisions.Collidable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexey.moroz on 3/21/18.
 */

public class PlayerCharacter implements RenderableEntity, UpdatableEntity, Disposable{

    public static final float CHAR_RADIUS = 0.1f;

    private int forceUId = 0;


    private ModelBatch modelBatch;
    private Model model;
    private ModelInstance modelInstance;

    public AnimationController controller;
    public Circle boundingCircle;

    private Vector2 prevPosition;
    private Vector2 position;
    public Vector2 velocity;
    public Vector2 acceleration;

    private float mass;

    private Map<Integer, Vector2> actingForces;



    private float scale;

    public PlayerCharacter(Model model) {
        this.modelBatch = new ModelBatch();

        this.model = model;
        this.modelInstance = new ModelInstance(model);
        for (Material material : modelInstance.materials) {
            material.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA));
        }
        this.controller = new AnimationController(modelInstance);


        this.actingForces = new HashMap<Integer, Vector2>();

        this.prevPosition = new Vector2();
        this.position = new Vector2();
        this.velocity = new Vector2();
        this.acceleration = new Vector2();

        this.scale = 1.0f;
        this.mass = 1.0f;

        this.boundingCircle = new Circle(0f,0f, CHAR_RADIUS);
    }

    public Vector2 getPrevPosition(){
        return prevPosition;
    }

    public Vector2 getPosition(){
        return this.position;
    }

    public float getScale(){
        return this.scale;
    }

    public void setScale(float scaleValue){
        this.scale = scaleValue;
        this.modelInstance.transform.setToScaling(this.scale, this.scale, this.scale);
    }


    public int addForce(Vector2 force){
        //record action force
        actingForces.put(forceUId, force);

        //return unique force Id
        forceUId++;
        return forceUId - 1;
    }


    public void removeForce(int forceUId){
        actingForces.remove(forceUId);
    }


    public Vector2 getForce(int forceUId){
        return actingForces.get(forceUId);
    }


    public int setForce(int forceUId, Vector2 newForceVec){

        if(actingForces.containsKey(forceUId)){
            actingForces.get(forceUId).set(newForceVec);
            return forceUId;
        }

        return addForce(newForceVec);
    }

    public Vector2 getResultantActingForce(){
        Vector2 resultingForce = new Vector2();

        for(Vector2 force: actingForces.values()){
            resultingForce.add(force);
        }

        return resultingForce;
    }


    public Vector2 getExclusiveResultantForce(int excludeForceUid){
        Vector2 resultingForce = getResultantActingForce();

        if(!actingForces.containsKey(excludeForceUid)){
            return resultingForce;
        }

        return resultingForce.sub( actingForces.get(excludeForceUid) );
    }


    public void move(float dx, float dy){
        move(new Vector2(dx, dy));
    }

    public void move(Vector2 moveVec2){
        setPosition(moveVec2.add(position));
    }

    public void setPosition(float posX, float posY){
        this.setPosition(new Vector2(posX, posY));
    }

    public void setPosition(Vector2 newPosition){
//        prevPosition.set(position);

        position.set(newPosition);

        updateModelAndCollisionShape();
    }


    private void updateModelAndCollisionShape(){
        //TODO: Z-order layering?
        modelInstance.transform.setTranslation(position.x, position.y, -0.01f);
        boundingCircle.setPosition(position);
    }


    @Override
    public void update(float deltaTime) {
        controller.update(deltaTime);

        Vector2 resultantForce = getResultantActingForce();

        acceleration.set(resultantForce.scl(1.0f/mass));

        velocity.mulAdd(acceleration, deltaTime);

        prevPosition.set(position);
        position.mulAdd(velocity, deltaTime);

        updateModelAndCollisionShape();
    }


    @Override
    public void render(Camera camera, Environment environment) {
        updateModelAndCollisionShape();

        this.modelBatch.begin(camera);
        this.modelBatch.render(this.modelInstance, environment);
        this.modelBatch.end();
    }

    @Override
    public void dispose() {
        this.model.dispose();
        this.modelBatch.dispose();
    }
}

