package com.supergdx.game.objects;

/**
 * Created by alexey.moroz on 3/21/18.
 */

public interface UpdatableEntity {
    void update(float deltaTime);
}
