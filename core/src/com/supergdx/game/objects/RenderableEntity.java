package com.supergdx.game.objects;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Environment;


/**
 * Created by alexey.moroz on 3/21/18.
 */

public interface RenderableEntity {
    void render(Camera camera, Environment environment);

}
