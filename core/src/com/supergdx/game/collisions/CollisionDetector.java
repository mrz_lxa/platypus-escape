package com.supergdx.game.collisions;

/**
 * Created by alexey.moroz on 4/7/18.
 */

public interface CollisionDetector {

    CollisionResult detectCollision(Collidable checkingObject, Collidable checkedObject);
}
