package com.supergdx.game.terrain;

import com.supergdx.game.math.FMath;
import com.supergdx.game.math.MFunction;
import com.supergdx.game.math.MFunctionSum;
import com.supergdx.game.math.functions.ComplexSineFunction;
import com.supergdx.game.math.functions.ShortestDistanceFunction;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey.moroz on 3/15/18.
 */

public class Terrain {

    private MFunction terrainFunction;
    private List<ComplexSineFunction> complexSinesList;
    private ShortestDistanceFunction shortestDistanceFunction;


    public Terrain(List<ComplexSineFunction> complexSinesList){

        List<MFunction> funcList = new ArrayList<MFunction>();
        funcList.addAll(complexSinesList);

        this.terrainFunction = new MFunctionSum(funcList);
        this.complexSinesList = complexSinesList;

        this.shortestDistanceFunction = new ShortestDistanceFunction(this.terrainFunction);
    }


    public MFunction getFunction(){
        return this.terrainFunction;
    }

    public ShortestDistanceFunction getShortestDistanceFunction(){
        return this.shortestDistanceFunction;
    }

    public float getHeightAt(float posX){
        return terrainFunction.val(posX);
    }


    public int getFunctionsCount(){
        return complexSinesList.size();
    }

    public float getFuncPeriodAt(float posX){
        Iterator<ComplexSineFunction> cSineIterator = complexSinesList.iterator();
        List<Float> periods = new LinkedList<Float>();

        while (cSineIterator.hasNext()){
            ComplexSineFunction cSineFunc = cSineIterator.next();

            periods.add(cSineFunc.getPeriodAt(posX));
        }


        return FMath.findLCM(periods);
    }

    public FloatBuffer getParamsBuffer(){
        int bytesToAllocate = 0;

        for(ComplexSineFunction compSineFunc: complexSinesList){
            bytesToAllocate += compSineFunc.getParamsBufferSize();
        }


        ByteBuffer bb = ByteBuffer.allocateDirect(bytesToAllocate);
        bb.order(ByteOrder.nativeOrder());

        FloatBuffer fb = bb.asFloatBuffer();


        for(ComplexSineFunction compSineFunc: complexSinesList){
            fb.put(compSineFunc.getParamsBuffer());
        }

        return fb;
    }

}
