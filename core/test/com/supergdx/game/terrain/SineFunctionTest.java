package com.supergdx.game.terrain;

import com.supergdx.game.math.functions.SineFunction;

import org.junit.Test;

import java.nio.FloatBuffer;

import static org.junit.Assert.*;

/**
 * Created by alexey.moroz on 3/20/18.
 */
public class SineFunctionTest {


    private SineFunction target;


    @Test
    public void shouldReturnZero() {
        target = new SineFunction(1.0f, 1.0f, 0.0f, 0.0f);

        assertEquals(0.0f, target.val(0.0f), 0.00000001f);
    }


    @Test
    public void shouldReturnConstValue() {
        target = new SineFunction(0.0f, 1.0f, 0.0f, 25.0f);

        assertEquals(25.0f, target.val(0.0f), 0.00000001f);
        assertEquals(25.0f, target.val(120.0f), 0.00000001f);
        assertEquals(25.0f, target.val(100500.0f), 0.00000001f);
    }


    @Test
    public void shouldReturnScaledValue() {
        target = new SineFunction(10.0f, 1.0f, 0.0f, 0.0f);

        assertEquals(10.0f, target.val((float)Math.PI/2.0f), 0.00000001f);
        assertEquals(-10.0f, target.val((float)(3.0f*Math.PI/2.0f)), 0.00000001f);
    }


    @Test
    public void shouldReturnOneWithShiftedPhase() {
        target = new SineFunction(1.0f, 1.0f, (float)Math.PI/2.0f, 0.0f);

        assertEquals(1.0f, target.val(0.0f), 0.00000001f);
    }


    @Test
    public void shouldCalcDerivativeCorrectly() {
        target = new SineFunction(15.0f, 0.10f, 5.0f, 200.0f);

        assertEquals(1.4868f, target.der(200.0f), 0.00001f);
    }


    @Test
    public void shouldCalcSecondOrderDerivativeCorrectly() {
        target = new SineFunction(33.0f, 5.0f, 100.0f, 123.0f);

        assertEquals(-638.4f, target.der2(321.0f), 0.1f);
    }


    @Test
    public void shouldReturnCorrectFloatBuffer() {

        target = new SineFunction(33.0f, 5.0f, 100.0f, 123.0f);

        FloatBuffer buffer = target.getParamsBuffer();

        assertEquals(4, buffer.limit());
        assertEquals(4, buffer.capacity());
        assertEquals(4, buffer.remaining());

        assertEquals(33.0f, buffer.get(), 0.000001f);
        assertEquals(5.0f, buffer.get(), 0.000001f);
        assertEquals(100.0f, buffer.get(), 0.000001f);
        assertEquals(123.0f, buffer.get(), 0.000001f);

        FloatBuffer anotherBuffer = target.getParamsBuffer();

        assertEquals(4, anotherBuffer.limit());
        assertEquals(4, anotherBuffer.capacity());
        assertEquals(4, anotherBuffer.remaining());
    }
}