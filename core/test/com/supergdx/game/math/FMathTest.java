package com.supergdx.game.math;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by alexey.moroz on 8/9/18.
 */
public class FMathTest {


    private final static float accDelta = 0.00001f;


    @Test
    public void shouldGCDCorrectly() {
        float res = FMath.gcd(0.48f, 0.108f);

        assertEquals(0.012f, res, accDelta);
    }


    @Test
    public void shouldCalcLCMCorrectly() {
        List<Float> floatList = Arrays.asList(2f, 7f, 3f, 9f, 4f);


        float res = FMath.findLCM(floatList);


        assertEquals(252.0f, res, accDelta);
    }


    @Test
    public void shouldCalcLCMCorrectly_two() {
        List<Float> floatList = Arrays.asList(1f, 2f, 8f, 3f);


        float res = FMath.findLCM(floatList);


        assertEquals(24.0f, res, accDelta);
    }

}